Summary: Python worker node micro-framework
Name: python3-wnfm
Version: 0.1.24
Release: 1%{?dist}
Source0: python-wnfm-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
%if 0%{?el7}
Requires: pexpect python2-future
%else
Requires: python3-pexpect
%endif
Url: https://gitlab.cern.ch/etf/wn-fm
%if 0%{?el7}
BuildRequires: python-setuptools
%else
BuildRequires: python3-setuptools python3-devel /usr/bin/pathfix.py
%endif
%description

ETF WN-uFM - ETF worker node micro-framework runs
nagios/monitoring plugins in parallel and has configurable
set of backends to support publishing the results


%prep
%autosetup -n python-wnfm-%{version}
%if 0%{?el7}
%else
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" . bin/etf_wnfm
%endif

%build
%if 0%{?el7}
python setup.py build
%else
%py3_build
%endif

%install
%if 0%{?el7}
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%else
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.md
