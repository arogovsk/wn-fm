```
ETF WN-uFM - ETF worker node micro-framework, runs nagios/monitoring plugins or
general purpose scripts (in parallel), collects the results and has configurable 
set of backends to support publishing of the results. 
Compatible with py2.6+; requires only two libraries (pexpect, ptyprocess)

usage: etf_wnfm [-h] [--version] [-p POOL_SIZE] [-i PLUGIN_DIR] [-o OUT_DIR]
                [-b BACKEND] [--prefix PREFIX] [--suffix SUFFIX] [-d]
                [-t TIMEOUT] [--backend-timeout B_TIMEOUT]
                [--status-map STATUS_MAP] [--service-uri SERVICE_URI]
                [--pid-file PID_FILE]

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -p POOL_SIZE, --pool POOL_SIZE
                        Number of sub-processes to run in parallel (defaults
                        to 2); setting this to 1 implies sequential execution
  -i PLUGIN_DIR, --plugin-directory PLUGIN_DIR
                        Directory containing plugins to execute
  -o OUT_DIR, --output-directory OUT_DIR
                        Directory to store inter-mediate results
  -b BACKEND, --backend BACKEND
                        Backend to execute for each plugin
  -d, --debug           Specify debugging mode
  -t TIMEOUT, --plugin-timeout TIMEOUT
                        Plugin execution timeout
  --backend-timeout B_TIMEOUT
                        Backend execution timeout
  --status-map STATUS_MAP
                        How return codes should be mapped to status (nagios,
                        old_sam; default is nagios)
  --service-uri SERVICE_URI
                        Service uri identifying the originating host
                        (host[:port]/[resource])
  --pid-file PID_FILE   Write the process ID in the specified file
  
                        
ETF WN-uFM can be used directly from python via API, e.g.
import wnfm.core

def wn_fm_pool(plugin_dir, out_dir, backend, plugin_timeout=600, pool_size=2,
               backend_timeout=120, extra=None, status_map='nagios', service_uri=None):
    """
    Searches for tests/plugins in the plugin directory, runs them in parallel using a process pool,
    stores results in output directory and calls configured backends to process the
    output directory further. Pool size is configurable and defaults to 2 to ensure
    all plugins will run even if one of them gets blocked. Timeouts for plugin
    and backend execution are supported. Plugin execution relies on the availability of
    pexpect module.

    :param plugin_dir:  Directory containing plugins to execute
    :param out_dir: Directory to store plugin results (output)
    :param backend: Backend to call after executing plugin
    :param plugin_timeout: Plugin execution timeout (default is 5 minutes)
    :param backend_timeout: Backend execution timeout (default is 2 minutes)
    :param pool_size: Number of plugins to run in parallel (controls size of the process pool, defaults to 2)
    :param extra: Any extra configuration that will be passed to backend
    :param status_map: How return codes should be mapped to status (nagios; old_sam; default is nagios)
    :param service_uri: Service uri identifying the originating host (host[:port]/[resource])
    :return: once the pool can join
    """
```