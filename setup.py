from setuptools import setup

import wnfm

NAME = 'python-wnfm'
VERSION = wnfm.VERSION
DESCRIPTION = "Python ETF worker node micro-framework"
LONG_DESCRIPTION = """
ETF WN-uFM - ETF worker node micro-framework runs
nagios/monitoring plugins in parallel and has configurable
set of backends to support publishing the results
"""
AUTHOR = wnfm.AUTHOR
AUTHOR_EMAIL = wnfm.AUTHOR_EMAIL
LICENSE = "ASL 2.0"
PLATFORMS = "Any"
URL = "https://gitlab.cern.ch/etf/wn-fm"
CLASSIFIERS = [
    "Development Status :: 5 - Production/Stable",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: Unix",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.0",
    "Programming Language :: Python :: 3.1",
    "Programming Language :: Python :: 3.2",
    "Programming Language :: Python :: 3.3",
    "Topic :: Software Development :: Libraries :: Python Modules"
]

setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      license=LICENSE,
      platforms=PLATFORMS,
      url=URL,
      classifiers=CLASSIFIERS,
      keywords='operations python WN framework nagios',
      packages=['wnfm', 'wnfm.backends'],
      install_requires=['pexpect'],
      data_files=[
          ('/usr/bin', ['bin/etf_wnfm', 'bin/etf_dummy.sh', 'bin/etf_run.sh'])]
      )
