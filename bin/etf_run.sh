#!/bin/sh
#########################################################################################
#
# NAME:        etf_run.sh
#
# FACILITY:    Experiments Test Framework (ETF) - Worker Node micro-Framework (WN-qFM) -
#              part of WLCG SAM (Service Availability Monitoring)
#
# DESCRIPTION:
#
#         WN script to set up required environment, launch and monitor ETF WN-qFM
#
# AUTHORS:     Marian Babik, CERN
# based on nagrun.sh written in 2008 by Konstantin Skaburskas, CERN (now @SixSq)
#
##########################################################################################

# Set en_US.UTF-8 locale
for l in $(locale);do export ${l//=*/}="en_US.UTF-8";done

VO=
VO_FQAN=
ETFLIFETIME=600
ETFTESTTIME=570
FW_VERB=0
ETF_RUN=true
ETF_STATIC=false
SITE=
CE_NAME=localhost
THREADS=2

usage="usage: $(basename $0) -v <vo> -c <ce_name>] [-t <global_timeout>] [-d <fw_verb>] \n
                            [-e site] [-f <fqan>] [-N] [-h] [-p threads] [-T <test_timeout>]\n
-v and -c are mandatory parameters. Defaults:\n
-t <timeout> - global timeout - $ETFLIFETIME sec\n
-T <timeout> - pre test timeout - $ETFTESTTIME sec\n
-p <threads> - $THREADS\n
-d <fw_verb> - enable debug - $FW_VERB\n
-f <fqan> - VOMS FQAN\n
-N - don't run WN tests\n
-e <site_name>\n
-s - use statically compiled ETF WN-qFM\n
"

while getopts ":p:t:v:f:e:c:T:Ndhs" options; do
  case ${options} in
    t ) ETFLIFETIME=$OPTARG;;
    T ) ETFTESTTIME=$OPTARG;;
    v ) VO=$OPTARG;;
    f ) VO_FQAN=$OPTARG;;
    d ) FW_VERB=1;;
    e ) SITE=$OPTARG;;
    c ) CE_NAME=$OPTARG;;
    p ) THREADS=$OPTARG;;
    N ) ETF_RUN=false;;
    s ) ETF_STATIC=true;;
    : ) echo "Invalid Option: -$OPTARG requires an argument" 1>&2
        exit 1;;
    \? )
        echo "Invalid Option: -$OPTARG" 1>&2
        exit 1;;
    h ) echo -e $usage
        exit 1;;
    * ) echo -e $usage
        exit 1;;
  esac
done

if [ -z "$VO" ] ; then
  echo -e $usage
  exit 1
fi

echo "Launched with parameters: $@"
echo "Shell version:"
$SHELL --version

echo "VO=$VO"
echo "VO_FQAN=$VO_FQAN"
echo "ETFLIFETIME=$ETFLIFETIME"
echo "ETFTESTTIME=$ETFTESTTIME"
echo "ETF_RUN=$ETF_RUN"
echo "FW_VERB=$FW_VERB"
echo "CE_NAME=$CE_NAME"
echo "SITE=$SITE"

trap _on_exit EXIT

function _on_exit {
    echo "Exiting ..."
    rm -rf $ETFROOT $FW_TMP
}

function _package_wnlogs() {
    tar -c $ETFVAR $ETF_OUT_DIR | gzip > $WNLOGS
}

#
ARCH=$(uname -m)
JWD=$(pwd)
export ETFROOT=$JWD/etf
ETFVAR=$ETFROOT/var
FW_TMP=/tmp/sam.$$.$RANDOM
export ETF_OUT_DIR=$FW_TMP/msg-outgoing

mkdir -p "$ETFROOT" "$ETFVAR" "$FW_TMP"
if [ $? -ne 0 ]; then
    echo "CRITICAL: Failed creating directories needed for ETF:"
    echo "    $ETFROOT $ETFVAR $ETFTMP"
    exit 1
fi

# tarball with log files to go into OutputSandbox
WNLOGS=$JWD/wnlogs.tgz
touch $WNLOGS

#if [ $FW_VERB -ge 1 ]; then
#   /bin/find . -print
#   /bin/env
#fi

if [ ! -f gridjob.tgz ] ; then
   # Unable to find gridjob.tgz in the current directory
   # Trying to lookup
   echo "Unable to find gridjob.tgz in $JWD, trying to search"
   if [ -n "SCRATCH_DIRECTORY" ] && [ -f $SCRATCH_DIRECTORY/gridjob.tgz ]; then
      GRIDJOB_PATH=$SCRATCH_DIRECTORY/gridjob.tgz
   else
      GRIDJOB_PATH=`find ~/ -name gridjob.tgz -print | head -n 1`
   fi
   if [ -z "$GRIDJOB_PATH" ]; then
       echo "Unable to find input file gridjob.tgz"
       exit $RC_UNKNOWN
   fi
   # Copy over to the current directory
   echo "Input file found at: $GRIDJOB_PATH"
   cp -f $GRIDJOB_PATH .
fi

gunzip -f gridjob.tgz && tar xf gridjob.tar -C $ETFROOT

mkdir -p "$ETFVAR"

# setup env
if [ -f $ETFROOT/etf-env.sh ] ; then
    set -a
    source $ETFROOT/etf-env.sh
    set +a
else
    echo "Unable to find etf-env.sh in $JWD, bailing out"
    exit 1
fi

echo "ETF environment:"
env | grep "ETF"

# python WN-qFM, pexpect and pty packages will become available here
export PYTHONPATH=$ETFROOT/lib/python/site-packages:$ETFROOT/probes:$PYTHONPATH

# 64bit Python loading 32bit libs
#if [ "x${ARCH}" == "xx86_64" ] ; then
#   if ( ! /usr/bin/env python -c "import _lcg_util" > /dev/null 2>&1 ) ; then
#      # Try to change to 32bit Python from SW area
#      PYTHON=PYTHON-2.4.6-i386
#      PYPATH=$VO_OPS_SW_DIR/$PYTHON
#      [ -f $PYPATH/env.sh ] && . $PYPATH/env.sh
#   fi
#fi

export SITE_NAME=$SITE

# Ensure that we'll have this info.
echo "=== [$(date -u)] ==="
echo "=== Running on ==="
echo "=== Site: $SITE_NAME"
echo "=== CE: $CE_NAME"
echo "=== WN: $(hostname)"
echo "=== WN arch: ${ARCH}"


if [ $FW_VERB -ge 1 ]; then
  echo "Looking for Python ..."
  ETF_PYTHON=$(command -v python)
  if [ $? -ne 0 ]; then
    ETF_PYTHON=$(command -v python2)
    if [ $? -ne 0 ]; then
      ETF_PYTHON=$(command -v python3)
      if [ $? -ne 0 ]; then
        echo "Python not found, please contact ETF support (unable to run WN tests)"
        exit 1
      fi
    fi
  fi
  echo "Python:"
  echo "${ETF_PYTHON}"

  echo "Check Python version:"
  ${ETF_PYTHON} -V 2>&1

  echo -e "Can we import ETF WN-qFM ... "
  ${ETF_PYTHON} -c "import wnfm" && echo "YES." || echo "NO."
  echo -e "Can we import python pexpect ... "
  ${ETF_PYTHON} -c "import pexpect" && echo "YES." || echo "NO."
  echo -e "Can we import python json ... "
  ${ETF_PYTHON} -c "import json" && echo "YES." || echo "NO."
fi


if [ "$ETF_RUN" = false ]; then
  ${ETF_PYTHON} "$ETFROOT"/bin/etf_wnfm --help
	echo "ATTENTION: Asked not to run ETF WN-qFM. Bailing out."
	exit 0
fi

# Launch ETF WN-qFM
if [ $FW_VERB -ge 1 ]; then
  ETF_DEBUG='-d'
else
  ETF_DEBUG=''
fi
ETFARGS="-p $THREADS -i $ETFROOT/probes -o $ETF_OUT_DIR -t $ETFTESTTIME -b jsonify --pid-file $ETFVAR/etf.pid $ETF_DEBUG --service-uri $CE_NAME"
echo "                      $ETFROOT/bin/etf_wnfm $ETFARGS"
ulimit -s 10240
if [ "$ETF_STATIC" = true ]; then
  echo "Launching static ETF WN-qFM: $(date -u)"
  "$ETFROOT"/libexec/etf_wnfm -s $ETFARGS >"$ETFVAR"/etf_wnfm.log 2>&1 &
else
  echo "Launching ETF WN-qFM: $(date -u)"
  ${ETF_PYTHON} "$ETFROOT"/bin/etf_wnfm $ETFARGS >"$ETFVAR"/etf_wnfm.log 2>&1 &
fi
if [ $? -ne 0 ]; then
  echo "ERROR: Couldn't launch ETF WN-qFM! "
  cat $ETFVAR/etf_wnfm.log
  exit 1
fi
sleep 60
if [ ! -f $ETFVAR/etf.pid ]; then
  echo "Unable to find ETF WN-qFM pid file $ETFVAR/etf.pid"
  cat $ETFVAR/etf_wnfm.log
  exit 1
fi

etf_pid=$(<"$ETFVAR/etf.pid")
echo "ETF WN-qFM pid: $etf_pid"
ETFTIMESTOP=$(($(date +%s) + $ETFLIFETIME))

while ( $(ps -p $etf_pid > /dev/null) ); do
 	if [ $(date +%s) -ge $ETFTIMESTOP ] ; then
		echo "High level timeout $ETFLIFETIME sec reached. [$(( $ETFTIMESTOP - $(date +%s) ))] Killing ETF WN-qFM."
		kill -15 $etf_pid && echo "Attempting graceful shutdown (sent SIGTERM)."
		sleep 25
		if [ $(ps -p $etf_pid > /dev/null) ]; then
		    kill -9 $etf_pid && echo "ETF was successfully killed."
		fi
	fi
	sleep 2
done

_package_wnlogs
echo "ETF WN-qFM log:"
cat $ETFVAR/etf_wnfm.log
echo "================== $(date -u)"
exit 0