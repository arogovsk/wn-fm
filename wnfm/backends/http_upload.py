import logging
import os

HTTPLIB = False
try:
    import httplib
    HTTPLIB = True
except ImportError:
    try:
        import http.client as httplib
        HTTPLIB = True
    except ImportError:
        pass

from wnfm.core import status

log = logging.getLogger("wnfm")


def run(plugin, ret_code, out_file, pid, timestamp, config, summary=None):
    if not HTTPLIB:
        log.debug('   Unable to import HTTP library, http_uload backend disabled')
        return
    log.debug("    Reading plugin output %d" % pid)

    with open(out_file, 'r') as f:
        plugin_out = f.readlines()

    if not plugin_out:
        log.debug("   Failed to read plugin output file %s" % out_file)
        return

    # serialization and upload
    url = config['http_upload_url']
    ce_name = config['http_upload_ce_name']
    plugin = os.path.splitext(os.path.basename(plugin))
    host_port = url.split('/')[2]
    path = '/' + '/'.join(url.split('/')[3:]) + ce_name + '/' + plugin

    log.info('Putting %s Nagios output to https://%s%s' % (plugin, host_port, path))

    try:
        connection = httplib.HTTPSConnection(host=host_port,
                                             timeout=30,
                                             key_file=os.environ['X509_USER_PROXY'],
                                             cert_file=os.environ['X509_USER_PROXY'])
        # what about timestamp ?
        connection.request('PUT', path, str(status(ret_code) + '\n' + plugin_out))
    except Exception as e:
        log.error('PUT of %s Nagios output fails with %s' % (plugin, str(e)))
    else:
        result = connection.getresponse()
        if result.status / 100 == 2:
            log.info('PUT of %s Nagios output succeeds with %d %s' % (plugin, result.status, result.reason))
        else:
            log.error('PUT of %s Nagios output fails with %d %s' % (plugin, result.status, result.reason))
