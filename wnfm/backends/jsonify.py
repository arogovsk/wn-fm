import logging
import os
import json
import wnfm.core

log = logging.getLogger("wnfm")


def run(plugin, ret_code, out_file, pid, timestamp, config, summary=None):
    log.debug("    Reading plugin output %d:" % pid)
    with open(out_file, 'r') as f:
        plugin_out = f.readlines()

    # remove \r added by pexpect
    plugin_out = [line.replace('\r', '') for line in plugin_out]

    if not plugin_out:
        log.debug("   Failed to read plugin output file %s" % out_file)
        return

    log.debug(plugin_out)
    # serialization
    log.debug("    Serializing %d" % pid)
    plugin_struct = dict()
    plugin_struct['retcode'] = ret_code
    plugin_struct['status'] = wnfm.core.status(ret_code)
    plugin_struct['timestamp'] = timestamp
    plugin_struct['nagios_name'] = os.path.basename(plugin)
    plugin_struct['gatheredAt'] = config['hostname']
    plugin_struct['service_uri'] = config['service_uri']
    if len(plugin_out) > 1:
        plugin_struct['summary'] = '{0}'.format(plugin_out[0]).rstrip()
    else:
        plugin_struct['summary'] = 'Test produced no output'
    if summary:
        plugin_struct['summary'] = summary
    if summary and plugin_out:
        plugin_struct['details'] = ''.join(plugin_out)
    elif len(plugin_out) >= 2:
        plugin_struct['details'] = ''.join(plugin_out[1:])
    else:
        plugin_struct['details'] = ''
    with open(os.path.join(config['output_dir'], os.path.splitext(os.path.basename(plugin))[0]+'.json'), 'w') as f:
        json.dump(plugin_struct, f)
