AUTHOR = "Marian Babik"
AUTHOR_EMAIL = "<marian.babik@cern.ch>"
COPYRIGHT = "Copyright (C) 2016 CERN"
VERSION = "0.1.24"
DATE = "24 Nov 2016"
__author__ = AUTHOR
__version__ = VERSION
__date__ = DATE


import logging

class NullHandler(logging.Handler):
    def handle(self, record):
        pass

    def emit(self, record):
        pass

    def createLock(self):
        self.lock = None
